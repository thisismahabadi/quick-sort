# Quick Sort

This simple quick sort app is implemented in Python.

# Usage

First, You need to download or clone the project:

```bash
git clone git@gitlab.com:thisismahabadi/quick-sort.git
```

p.s: Notice that Git is already installed on your system before. xD

then, change your directory to the project, like:

```bash
cd quick-sort
```

and, again, please check if Python is installed on your system and then run:

```bash
python index.py
```